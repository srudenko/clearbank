### Prerequisits

* .NET Core SDK 3.1

### Unit tests

The solution contains 21 unit-test.

Execute `dotnet test -v normal` in folder `ClearBank.DeveloperTest.Tests`

The expected result as below:



```
  V ClearBank.DeveloperTest.Tests.BacsPaymentProviderTests.Validate_Success_Should_allow_negative_balance [3ms]
  V ClearBank.DeveloperTest.Tests.ChapsPaymentProviderTests.Validate_Success [3ms]
  V ClearBank.DeveloperTest.Tests.FasterPaymentsPaymentProviderTests.Validate_Failed_Should_not_allow_negative_balance [3ms]
  V ClearBank.DeveloperTest.Tests.FasterPaymentsPaymentProviderTests.Validate_Success [< 1ms]
  V ClearBank.DeveloperTest.Tests.FasterPaymentsPaymentProviderTests.Validate_Failed_When_account_not_provided [< 1ms]
  V ClearBank.DeveloperTest.Tests.BacsPaymentProviderTests.Validate_Failed_When_Bacs_payment_not_allowed [< 1ms]
  V ClearBank.DeveloperTest.Tests.ChapsPaymentProviderTests.Validate_Failed_When_Chaps_payment_not_allowed [< 1ms]
  V ClearBank.DeveloperTest.Tests.FasterPaymentsPaymentProviderTests.Validate_Failed_When_FasterPayments_payment_not_allowed [< 1ms]
  V ClearBank.DeveloperTest.Tests.BacsPaymentProviderTests.Validate_Success [< 1ms]
  V ClearBank.DeveloperTest.Tests.ChapsPaymentProviderTests.Validate_Success_Should_allow_negative_balance [< 1ms]
  V ClearBank.DeveloperTest.Tests.BacsPaymentProviderTests.Validate_Failed_When_account_not_provided [< 1ms]
  V ClearBank.DeveloperTest.Tests.ChapsPaymentProviderTests.Validate_Failed_When_account_not_provided [< 1ms]
  V ClearBank.DeveloperTest.Tests.ChapsPaymentProviderTests.Validate_Failed_Chaps_Should_not_allow_non_live_accounts [< 1ms]
  V ClearBank.DeveloperTest.Tests.AccountProviderFactoryTests.AccountProviderFactory_Should_resolve_correct_storage(storageName: "Main", expectedType: "AccountDataStore") [78ms]
  V ClearBank.DeveloperTest.Tests.AccountProviderFactoryTests.AccountProviderFactory_Should_resolve_correct_storage(storageName: "Backup", expectedType: "BackupAccountDataStore") [8ms]
  V ClearBank.DeveloperTest.Tests.AccountProviderFactoryTests.AccountProviderFactory_Should_resolve_correct_storage(storageName: null, expectedType: "AccountDataStore") [1ms]
  V ClearBank.DeveloperTest.Tests.AccountServiceTests.GetAccount_Should_call_internal_storage [102ms]
  V ClearBank.DeveloperTest.Tests.AccountServiceTests.UpdateAccount_Should_call_internal_storage [2ms]
  V ClearBank.DeveloperTest.Tests.PaymentServiceTests.MakePayment_Failed_Should_not_allow_empty_account [105ms]
  V ClearBank.DeveloperTest.Tests.PaymentServiceTests.MakePayment_Success_Should_decrease_account_balance [5ms]
  V ClearBank.DeveloperTest.Tests.PaymentServiceTests.MakePayment_Failed_Should_validate_account [3ms]


Test Run Successful.
Total tests: 21
     Passed: 21
```



