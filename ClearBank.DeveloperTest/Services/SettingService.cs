using System.Configuration;

namespace ClearBank.DeveloperTest.Services
{
    public class SettingService : ISettingService
    {
        public string GetDataStoreType()
        {
            return ConfigurationManager.AppSettings[Constants.AppSettings.DataStoreType];
        }
    }
}
