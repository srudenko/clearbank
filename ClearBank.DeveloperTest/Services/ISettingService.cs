namespace ClearBank.DeveloperTest.Services
{
    public interface ISettingService
    {
        string GetDataStoreType();
    }
}
