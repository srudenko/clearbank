using ClearBank.DeveloperTest.Data;

namespace ClearBank.DeveloperTest.Services.Account
{
    public class AccountStorageProvider : IAccountStorageProvider
    {
        private readonly ISettingService settingService;

        public AccountStorageProvider(ISettingService settingService)
        {
            this.settingService = settingService;
        }

        public IAccountDataStore GetStorage()
        {
            if (settingService.GetDataStoreType() == Constants.AccountStore.Backup)
            {
                return new BackupAccountDataStore();
            }

            return new AccountDataStore();
        }
    }
}
