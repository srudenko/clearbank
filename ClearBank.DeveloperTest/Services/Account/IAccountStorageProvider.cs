using ClearBank.DeveloperTest.Data;

namespace ClearBank.DeveloperTest.Services.Account
{
    public interface IAccountStorageProvider
    {
        IAccountDataStore GetStorage();
    }
}
