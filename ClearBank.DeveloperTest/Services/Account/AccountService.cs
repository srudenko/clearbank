using ClearBank.DeveloperTest.Data;

namespace ClearBank.DeveloperTest.Services.Account
{
    public class AccountService : IAccountService
    {
        private readonly IAccountStorageProvider storageStorageProvider;

        public AccountService(IAccountStorageProvider storageStorageProvider)
        {
            this.storageStorageProvider = storageStorageProvider;
        }

        public Types.Account GetAccount(string accountNumber)
        {
            return CurrentStorage.GetAccount(accountNumber);
        }

        public void UpdateAccount(Types.Account account)
        {
            CurrentStorage.UpdateAccount(account);
        }

        private IAccountDataStore CurrentStorage => storageStorageProvider.GetStorage();
    }
}
