using System;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public class PaymentProviderService : IPaymentProviderService
    {
        public IPaymentProvider GetProvider(PaymentScheme paymentScheme)
        {
            return paymentScheme switch
            {
                PaymentScheme.Bacs => new BacsPaymentProvider(),
                PaymentScheme.FasterPayments => new FasterPaymentsPaymentProvider(),
                PaymentScheme.Chaps => new ChapsPaymentProvider(),
                _ => throw new Exception($"Payment provider not supported: {paymentScheme}")
            };
        }
    }
}
