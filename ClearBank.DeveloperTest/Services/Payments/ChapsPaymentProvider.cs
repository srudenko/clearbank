using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public class ChapsPaymentProvider: IPaymentProvider
    {
        public MakePaymentResult Validate(Types.Account account, decimal balance)
        {
            if (account == null)
            {
                return MakePaymentResult.Failed("Account not provided");
            }

            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Chaps))
            {
                return MakePaymentResult.Failed($"Payment not allowed: {AllowedPaymentSchemes.Chaps}");
            }

            if (account.Status != AccountStatus.Live)
            {
                return MakePaymentResult.Failed($"Invalid account status: {account.Status}");
            }

            return MakePaymentResult.Valid();
        }
    }
}
