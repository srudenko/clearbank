using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public interface IPaymentProvider
    {
        MakePaymentResult Validate(Types.Account account, decimal balance);
    }
}
