using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public interface IPaymentProviderService
    {
        IPaymentProvider GetProvider(PaymentScheme paymentScheme);
    }
}
