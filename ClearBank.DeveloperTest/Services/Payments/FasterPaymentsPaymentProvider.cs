using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public class FasterPaymentsPaymentProvider: IPaymentProvider
    {
        public MakePaymentResult Validate(Types.Account account, decimal balance)
        {
            if (account == null)
            {
                return MakePaymentResult.Failed("Account not provided");
            }

            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.FasterPayments))
            {
                return MakePaymentResult.Failed($"Payment not allowed: {AllowedPaymentSchemes.FasterPayments}");
            }

            if (account.Balance < balance)
            {
                return MakePaymentResult.Failed($"Not enough balance. Required: {balance}, Account Balance: {account.Balance}");
            }

            return MakePaymentResult.Valid();
        }
    }
}
