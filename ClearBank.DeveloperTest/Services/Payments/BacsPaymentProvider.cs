using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services.Payments
{
    public class BacsPaymentProvider: IPaymentProvider
    {
        public MakePaymentResult Validate(Types.Account account, decimal balance)
        {
            if (account == null)
            {
                return MakePaymentResult.Failed("Account not provided");
            }

            if (!account.AllowedPaymentSchemes.HasFlag(AllowedPaymentSchemes.Bacs))
            {
                return MakePaymentResult.Failed($"Payment not allowed: {AllowedPaymentSchemes.Bacs}");
            }

            return MakePaymentResult.Valid();
        }
    }
}
