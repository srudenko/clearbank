﻿using ClearBank.DeveloperTest.Services.Account;
using ClearBank.DeveloperTest.Services.Payments;
using ClearBank.DeveloperTest.Types;

namespace ClearBank.DeveloperTest.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IAccountService accountService;
        private readonly IPaymentProviderService paymentProviderService;

        public PaymentService(IAccountService accountService,
            IPaymentProviderService paymentProviderService)
        {
            this.accountService = accountService;
            this.paymentProviderService = paymentProviderService;
        }

        public MakePaymentResult MakePayment(MakePaymentRequest request)
        {
            var account = accountService.GetAccount(request.DebtorAccountNumber);

            if (account == null)
            {
                return MakePaymentResult.Failed($"Account not found: {request.DebtorAccountNumber}");
            }

            var paymentProvider = paymentProviderService.GetProvider(request.PaymentScheme);

            var result = paymentProvider.Validate(account, request.Amount);

            if (!result.Success)
            {
                return result;
            }

            account.Balance -= request.Amount;

            accountService.UpdateAccount(account);

            return result;
        }
    }
}
