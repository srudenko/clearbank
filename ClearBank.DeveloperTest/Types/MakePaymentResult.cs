﻿using System.Diagnostics.CodeAnalysis;

namespace ClearBank.DeveloperTest.Types

{
    [ExcludeFromCodeCoverage]
    public class MakePaymentResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }

        public static MakePaymentResult Failed(string validationMessage)
        {
            return new MakePaymentResult{ Success = false, ErrorMessage = validationMessage};
        }

        public static MakePaymentResult Valid()
        {
            return new MakePaymentResult{ Success = true};
        }
    }
}
