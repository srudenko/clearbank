﻿using System.Diagnostics.CodeAnalysis;

namespace ClearBank.DeveloperTest.Types
{
    [ExcludeFromCodeCoverage]
    public class Account
    {
        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public AccountStatus Status { get; set; }
        public AllowedPaymentSchemes AllowedPaymentSchemes { get; set; }

    }
}
