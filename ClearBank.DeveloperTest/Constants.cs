using System.Diagnostics.CodeAnalysis;

namespace ClearBank.DeveloperTest
{
    [ExcludeFromCodeCoverage]
    public static class Constants
    {
        public static class AccountStore
        {
            public const string Backup = "Backup";
        }

        public static class AppSettings
        {
            public static string DataStoreType = "DataStoreType";
        }
    }
}
