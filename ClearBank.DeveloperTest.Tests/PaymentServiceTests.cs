using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Services.Account;
using ClearBank.DeveloperTest.Services.Payments;
using ClearBank.DeveloperTest.Types;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class PaymentServiceTests
    {
        private Mock<IAccountService> accountServiceMock;
        private Mock<IPaymentProviderService> paymentProviderServiceMock;
        private PaymentService service;
        private string defaultAccountNumber = "1234";
        private Account defaultAccount;
        private Mock<IPaymentProvider> paymentProviderMock;

        public PaymentServiceTests()
        {
            accountServiceMock = new Mock<IAccountService>();
            paymentProviderServiceMock = new Mock<IPaymentProviderService>();
            paymentProviderMock = new Mock<IPaymentProvider>();

            paymentProviderServiceMock.Setup(x => x.GetProvider(It.IsAny<PaymentScheme>()))
                .Returns(paymentProviderMock.Object);


            service = new PaymentService(accountServiceMock.Object, paymentProviderServiceMock.Object);

            SetupDefaultMocks();
        }


        [Fact]
        public void MakePayment_Success_Should_decrease_account_balance()
        {
            //Arrange
            var paymentRequest = new MakePaymentRequest
            {
                DebtorAccountNumber = defaultAccountNumber,
                Amount = 50
            };

            //Act
            var actualResult = service.MakePayment(paymentRequest);

            //Assert
            Assert.True(actualResult.Success);

            var expectedBalance = defaultAccount.Balance = paymentRequest.Amount; // 200 - 50 = 150
            accountServiceMock.Verify(x=> x.UpdateAccount(It.Is<Account>(t => t.Balance == expectedBalance)));
        }

        [Fact]
        public void MakePayment_Failed_Should_not_allow_empty_account()
        {
            //Arrange
            var paymentRequest = new MakePaymentRequest
            {
                DebtorAccountNumber = defaultAccountNumber,
                Amount = 50
            };

            accountServiceMock.Setup(x => x.GetAccount(defaultAccountNumber)).Returns((Account) null);

            //Act
            var actualResult = service.MakePayment(paymentRequest);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Account not found: 1234", actualResult.ErrorMessage);

        }

        [Fact]
        public void MakePayment_Failed_Should_validate_account()
        {
            //Arrange
            var paymentRequest = new MakePaymentRequest
            {
                DebtorAccountNumber = defaultAccountNumber,
                Amount = 50
            };

            paymentProviderMock.Setup(x => x.Validate(It.IsAny<Account>(), It.IsAny<decimal>()))
                .Returns(() => MakePaymentResult.Failed("Unable to validate account: 1234"));

            //Act
            var actualResult = service.MakePayment(paymentRequest);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Unable to validate account: 1234", actualResult.ErrorMessage);
        }

        private void SetupDefaultMocks()
        {
            defaultAccount = new Account
            {
                Balance = 200,
                AccountNumber = defaultAccountNumber
            };

            accountServiceMock.Setup(x => x.GetAccount(defaultAccountNumber)).Returns(defaultAccount);
            paymentProviderMock.Setup(x => x.Validate(It.IsAny<Account>(), It.IsAny<decimal>())).Returns(MakePaymentResult.Valid);

        }

    }
}
