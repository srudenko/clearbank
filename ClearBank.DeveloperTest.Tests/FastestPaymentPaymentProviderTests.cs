using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Services.Payments;
using ClearBank.DeveloperTest.Types;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class FasterPaymentsPaymentProviderTests: PaymentProviderTests
    {
        public FasterPaymentsPaymentProviderTests()
        {
            paymentProvider = new FasterPaymentsPaymentProvider();
        }

        [Fact]
        public void Validate_Success()
        {
            //Arrange
            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                Balance = 200
            };

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.True(actualResult.Success);
        }

        [Fact]
        public void Validate_Failed_Should_not_allow_negative_balance()
        {
            //Arrange
            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.FasterPayments,
                Balance = 50
            };

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal($"Not enough balance. Required: 100, Account Balance: 50", actualResult.ErrorMessage);
        }

        [Fact]
        public void Validate_Failed_When_account_not_provided()
        {
            //Arrange

            //Act
            var actualResult = paymentProvider.Validate(null, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Account not provided", actualResult.ErrorMessage);
        }

        [Fact]
        public void Validate_Failed_When_FasterPayments_payment_not_allowed()
        {
            //Arrange
            var account = new Account();

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Payment not allowed: FasterPayments", actualResult.ErrorMessage);
        }


    }
}
