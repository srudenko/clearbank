using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Services.Account;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class AccountProviderFactoryTests
    {
        private Mock<ISettingService> settingServiceMock;

        [Theory]
        [InlineData("Backup", nameof(BackupAccountDataStore))]
        [InlineData("Main", nameof(AccountDataStore))]
        [InlineData(null, nameof(AccountDataStore))]
        public void AccountProviderFactory_Should_resolve_correct_storage(string storageName, string expectedType)
        {
            //Arrange
            settingServiceMock = new Mock<ISettingService>();
            var factory = new AccountStorageProvider(settingServiceMock.Object);

            settingServiceMock.Setup(x => x.GetDataStoreType()).Returns(storageName);

            //Act
            var actualStorage = factory.GetStorage();

            //Assert
            Assert.Equal(expectedType, actualStorage.GetType().Name);
        }
    }
}
