using ClearBank.DeveloperTest.Data;
using ClearBank.DeveloperTest.Services.Account;
using ClearBank.DeveloperTest.Types;
using Moq;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class AccountServiceTests
    {
        private Mock<IAccountStorageProvider> accountStorageProviderMock;
        private AccountService service;
        private Mock<IAccountDataStore> accountDataStoreMock;

        public AccountServiceTests()
        {
            accountStorageProviderMock = new Mock<IAccountStorageProvider>();
            accountDataStoreMock = new Mock<IAccountDataStore>();

            accountStorageProviderMock.Setup(x => x.GetStorage()).Returns(accountDataStoreMock.Object);

            service =  new AccountService(accountStorageProviderMock.Object);
        }

        [Fact]
        public void GetAccount_Should_call_internal_storage()
        {
            //Arrange
            var account = new Account();
            accountDataStoreMock.Setup(x => x.GetAccount("1234")).Returns(account);

            //Act
            var actualResult = service.GetAccount("1234");

            //Assert
            Assert.Equal(account, actualResult);
            accountDataStoreMock.Verify(x=>x.GetAccount("1234"),Times.Once);
        }

        [Fact]
        public void UpdateAccount_Should_call_internal_storage()
        {
            //Arrange
            var account = new Account();

            //Act
            service.UpdateAccount(account);

            //Assert
            accountDataStoreMock.Verify(x=>x.UpdateAccount(It.IsAny<Account>()),Times.Once);
        }

    }
}
