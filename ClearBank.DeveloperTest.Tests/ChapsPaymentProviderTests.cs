using ClearBank.DeveloperTest.Services;
using ClearBank.DeveloperTest.Services.Payments;
using ClearBank.DeveloperTest.Types;
using Xunit;

namespace ClearBank.DeveloperTest.Tests
{
    public class ChapsPaymentProviderTests: PaymentProviderTests
    {
        public ChapsPaymentProviderTests()
        {
            paymentProvider = new ChapsPaymentProvider();
        }

        [Fact]
        public void Validate_Success()
        {
            //Arrange
            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Status = AccountStatus.Live
            };

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.True(actualResult.Success);
        }

        [Fact]
        public void Validate_Success_Should_allow_negative_balance()
        {
            //Arrange
            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Balance = 50
            };

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.True(actualResult.Success);
        }

        [Fact]
        public void Validate_Failed_When_account_not_provided()
        {
            //Arrange

            //Act
            var actualResult = paymentProvider.Validate(null, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Account not provided", actualResult.ErrorMessage);
        }

        [Fact]
        public void Validate_Failed_When_Chaps_payment_not_allowed()
        {
            //Arrange
            var account = new Account();

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Payment not allowed: Chaps", actualResult.ErrorMessage);
        }

        [Fact]
        public void Validate_Failed_Chaps_Should_not_allow_non_live_accounts()
        {
            //Arrange
            var account = new Account
            {
                AllowedPaymentSchemes = AllowedPaymentSchemes.Chaps,
                Status = AccountStatus.Disabled
            };

            //Act
            var actualResult = paymentProvider.Validate(account, 100);

            //Assert
            Assert.False(actualResult.Success);
            Assert.Equal("Invalid account status: Disabled", actualResult.ErrorMessage);
        }

    }
}
